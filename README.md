# OSDBK Admin Console

The OSDBK Admin Console provides a convenient interface to query and manage the CPA database. Additionally, it also provides functionality to resend failed and expired events.

# OSDBK-Admin-Console & OSDBK

The OSDBK Admin Console is part of OSDBK (Open Source Dienst Beveiligd Koppelvlak). OSDBK consists of seven applications:

- EbMS Core / EbMS Admin
- JMS Producer
- JMS Consumer
- Apache ActiveMQ
- CPA Service
- OSDBK Admin Console
- Throttling Service (optional)

### OSDBK Admin Console Configuration

The cpa-service connection can be configured with the following parameters via the environment file:
~~~
export const environment = {
  production: false,
  apiUrl: 'http://localhost:8084/cpa', -- url to the cpa-service
  cpaAuthToken: 'xxxxxxx' -- basic auth token in Base64 format
};
~~~

The CPA Service features many endpoints that are used by the OSDBK Admin Console to manage the CPA database. These endpoints are protected by basic authentication and not exposed to other applications.

### How to run this application locally
An example docker-compose has been provided. This can be run to test this application.
However, the following variables need to be populated by means of a `.env` file:

~~~
${OSDBK_IMAGE_REPOSITORY} -- location of your Docker repository where you are housing your built OSDBK docker images
${ACTIVEMQ_IMAGE_TAG} -- the tag of your built ActiveMQ docker image
${ACTIVEMQ_POSTGRESQL_USERNAME} -- the username used to connect to the ActiveMQ database
${ACTIVEMQ_POSTGRESQL_PASSWORD} -- the password used to connect to the ActiveMQ database
${DGL_STUB_IMAGE_TAG}  -- the tag of your built stub application docker image, to simulate interactions with an external party
${DGL_STUB_MESSAGE_SENDER_OIN_AFNEMER} -- the OIN of your stubbed application
${EBMS_JDBC_USERNAME} -- the username used to connect to the EBMS database
${EBMS_JDBC_PASSWORD} -- the password used to connect to the EBMS database
${EBMS_JMS_BROKER_USERNAME} -- the username used to connect the ebms application to the ActiveMQ instance
${EBMS_JMS_BROKER_PASSWORD} -- the password used to connect the ebms application to the ActiveMQ instance
${EBMS_POSTGRESQL_IMAGE_TAG} -- the tag of your built EBMS PostgresQL docker image
${EBMS_STUB_IMAGE_TAG} -- the tag of your built stub application docker image, to simulate interactions with an external party
${JMS_CONSUMER_IMAGE_TAG} -- the tag of your built jms-consumer docker image
${JMS_CONSUMER_ACTIVEMQ_USER} -- the username used to connect the jms-consumer application to the ActiveMQ instance
${JMS_CONSUMER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-consumer application to the ActiveMQ instance
${JMS_PRODUCER_IMAGE_TAG} -- the tag of your built jms-producer docker image
${JMS_PRODUCER_ACTIVEMQ_USER} -- the username used to connect the jms-producer application to the ActiveMQ instance
${JMS_PRODUCER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-producer application to the ActiveMQ instance
${THROTTLING_SERVICE_IMAGE_TAG} -- the tag of your built throttling docker image
${CPA_SERVICE_IMAGE_TAG} -- the tag of your built cpa-service docker image
${OSDBK_ADMIN_CONSOLE_IMAGE_TAG} -- the tag of your built OSDBK Admin console docker image
${CPA_JDBC_USERNAME} -- the username used to connect to the CPA database
${CPA_JDBC_PASSWORD} -- the password used to connect to the CPA database
${CPA_SERVICE_ACTIVEMQ_USER} -- the username used to connect the cpa-service application to the ActiveMQ instance
${CPA_SERVICE_ACTIVEMQ_PASSWORD} -- the password used to connect the cpa-service application to the ActiveMQ instance
${CPA_DATABASE_IMAGE_TAG} -- the tag of your built CPA PostgresQL docker image
~~~

Example command to bring up the application:
~~~
docker-compose --env-file path/to/env/file/.env up
~~~

## Release notes
See [NOTES][NOTES] for latest.

[NOTES]: ../../../templates/NOTES.txt

### Older release notes collated below:

V Up to 1.12.3
- Various improvements to:
    - container technology
    - use of base-images
    - GITLAB Security scan framework implemented
    - Improved Open Source build process and container releases
    - Test improvements via docker-compose
    - Dependency upgrades
