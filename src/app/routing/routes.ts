import { Routes } from '@angular/router';

import { CpaActiveComponent } from '../view/cpa-active/cpa-active.component';
import { CpaInactiveComponent } from '../view/cpa-inactive/cpa-inactive.component';
import { CpaUploadComponent } from '../view/cpa-upload/cpa-upload.component';
import { CpaViewComponent } from '../view/cpa-view/cpa-view.component';
import { CpaErrorComponent } from '../view/cpa-error/cpa-error.component';

export const routes: Routes = [
    { path: '', redirectTo: '/active', pathMatch: 'full' },
    { path: 'active', component: CpaActiveComponent },
    { path: 'upload', component: CpaUploadComponent },
    { path: 'view/:id', component: CpaViewComponent },
    { path: 'inactive', component: CpaInactiveComponent },
    { path: '**', component: CpaErrorComponent }
  ];