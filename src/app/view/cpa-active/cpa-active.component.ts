import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { saveAs } from 'file-saver';

import { CPAService } from '../../shared/service/cpa.service';
import { ConfirmDialogComponent } from '../../shared/component/confirm-dialog/confirm-dialog.component';
import { flyInOut } from '../../shared/animation/app.animation';

@Component({
  selector: 'cpa-active',
  templateUrl: './cpa-active.component.html',
  styleUrls: ['./cpa-active.component.css'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut()
  ]
})

export class CpaActiveComponent implements OnInit  {

  allCpaResponse!: string[];

  allCpaErrorMessage!: string;
  deactivationErrorMessage!: string;
  deletionErrorMessage!: string;
  resendErrorMessage!: string;
  resendSuccessLabel!: string;
  pingErrorMessage!: string;
  pingSuccessLabel!: string;

  searchText!: string;

  page: number = 1;

  constructor(
    private cpaService: CPAService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.getAllActiveCPAIds();
  }

  getAllActiveCPAIds(): void {
    this.cpaService.getActiveCPAIds().subscribe({
      next: (response: string[]) => this.allCpaResponse = response,
      error: (errorMessage) => this.allCpaErrorMessage = errorMessage
    });
  }

  downloadCpa(cpaId: string) {
    this.cpaService.downloadCPA(cpaId).subscribe(data => saveAs(data, cpaId + ".xml"));
  }

  pingCpa0to1(cpaId: string) {
    this.pingCpa(cpaId, 0, 1);
  }
  pingCpa1to0(cpaId: string) {
    this.pingCpa(cpaId, 1, 0);
  }

  pingCpa(cpaId: string, fromPartyIdx: number, toPartyIdx: number) {
    this.resetLabelsToInitialValues();
    this.cpaService.pingCpa(cpaId, fromPartyIdx, toPartyIdx).subscribe({
      next: (response: string) => this.pingSuccessLabel = `Ping of Cpa ${cpaId} was successful: ${response}`,
      error: (errorMessage) => this.pingErrorMessage = errorMessage
    });
  }

  confirmAction(cpaId: string, action: string) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        message: action + ':' + cpaId + ' ?'
      }
    });

    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed && action === 'Deactivate') {
        this.deactivateCpa(cpaId);
      }
      else if (confirmed && action === 'Delete') {
        this.deleteCpa(cpaId);
      }
      else if (confirmed && action === 'Resend Failed') {
        this.resendFailedForCpa(cpaId);
      }
      else if (confirmed && action === 'Resend Expired') {
        this.resendExpiredForCpa(cpaId);
      }
    });
  }

  deactivateCpa(cpaId: string) {
    this.resetLabelsToInitialValues();
    this.cpaService.deactivateCPA(cpaId).subscribe({
      error: (errorMessage) => this.deactivationErrorMessage = errorMessage,
      complete: () => this.allCpaResponse = this.allCpaResponse?.filter(item => item !== cpaId)
    });
  }

  deleteCpa(cpaId: string) {
    this.resetLabelsToInitialValues();
    this.cpaService.deleteCPA(cpaId).subscribe({
      error: (errorMessage) => this.deletionErrorMessage = errorMessage,
      complete: () => this.allCpaResponse = this.allCpaResponse?.filter(item => item !== cpaId)
    });
  }

  resendFailedForCpa(cpaId: string) {
    this.resetLabelsToInitialValues();
    this.cpaService.resendFailed(cpaId).subscribe({
      error: (errorMessage) => this.resendErrorMessage = errorMessage,
      complete: () => this.resendSuccessLabel = "Resend Failed Cpa's was succesvol"
    });
  }

  resendExpiredForCpa(cpaId: string) {
    this.resetLabelsToInitialValues();
    this.cpaService.resendExpired(cpaId).subscribe({
      error: (errorMessage) => this.resendErrorMessage = errorMessage,
      complete: () => this.resendSuccessLabel = "Resend Expired Cpa's was succesvol"
    });
  }

  resetLabelsToInitialValues() {
    this.deactivationErrorMessage = "";
    this.deletionErrorMessage = "";
    this.resendErrorMessage = "";
    this.resendSuccessLabel = "";
    this.pingErrorMessage = "";
    this.pingSuccessLabel = "";
  }

}
