import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'

import { CPAParty } from '../../shared/model/cpa-model';
import { CPAService } from '../../shared/service/cpa.service';
import { flyInOut } from '../../shared/animation/app.animation';

@Component({
  selector: 'cpa-view',
  templateUrl: './cpa-view.component.html',
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut()
  ]
})

export class CpaViewComponent implements OnInit {

  cpaContentResponse!: string;
  cpaPartyResponse!: CPAParty[];
  cpaContentError!: string;
  cpaPartyError!: string;

  constructor(
    private cpaService: CPAService,
    private activatedRoute: ActivatedRoute,
    private location: Location) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];

      this.cpaService.getCPA(id).subscribe({
        next: (response: string) => this.cpaContentResponse = response,
        error: (errorMessage) => this.cpaContentError = errorMessage
      });

      this.cpaService.getCPAParty(id).subscribe({
        next: (response: CPAParty[]) => this.cpaPartyResponse = response,
        error: (errorMessage) => this.cpaPartyError = errorMessage
      });

    });
  }

  onBack() {
    this.location.back();
  }


}
