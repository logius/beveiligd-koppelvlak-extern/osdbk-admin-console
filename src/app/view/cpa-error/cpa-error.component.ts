import { Component, OnInit } from '@angular/core';
import { flyInOut } from '../../shared/animation/app.animation';

@Component({
  selector: 'cpa-error',
  templateUrl: './cpa-error.component.html',
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut()
  ]
})

export class CpaErrorComponent implements OnInit {

  title: string = "Pagina niet gevonden";
  subtitle: string = "404";
  content: string = "Sorry, deze pagina is niet beschikbaar";

  ngOnInit() { }
}
