import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { saveAs } from 'file-saver';

import { CPAService } from '../../shared/service/cpa.service';
import { ConfirmDialogComponent } from '../../shared/component/confirm-dialog/confirm-dialog.component';
import { flyInOut } from '../../shared/animation/app.animation';

@Component({
  selector: 'cpa-inactive',
  templateUrl: './cpa-inactive.component.html',
  styleUrls: ['./cpa-inactive.component.css'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut()
  ]
})

export class CpaInactiveComponent implements OnInit {

  allCpaResponse!: string[];

  allCpaErrorMessage!: string;
  activationErrorMessage!: string;
  deletionErrorMessage!: string ;

  searchText!: string;

  page: number = 1;

  constructor(
    private cpaService: CPAService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.getAllInactiveCPAIds();
  }

  getAllInactiveCPAIds(): void {
    this.cpaService.getInactiveCPAIds().subscribe({
      next: (response: string[]) => this.allCpaResponse = response,
      error: (errorMessage) => this.allCpaErrorMessage = errorMessage
    });
  }

  downloadCpa(cpaId: string) {
    this.cpaService.downloadCPA(cpaId).subscribe(data => saveAs(data, cpaId + ".xml"));
  }

  confirmAction(cpaId: string, action: string) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        message: action + ':' + cpaId + ' ?'
      }
    });

    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed && action === 'Delete') {
        this.deleteCpa(cpaId);
      }
      else if (confirmed && action === 'Activate') {
        this.activateCpa(cpaId);
      }
    });
  }

  activateCpa(cpaId: string) {
    this.resetLabelsToInitialValues();
    this.cpaService.activateCPA(cpaId).subscribe({
      error: (errorMessage) => this.activationErrorMessage = errorMessage,
      complete: () => this.allCpaResponse = this.allCpaResponse?.filter(item => item !== cpaId)
    });
  }

  deleteCpa(cpaId: string) {
    this.resetLabelsToInitialValues();
    this.cpaService.deleteCPA(cpaId).subscribe({
      error: (errorMessage) => this.deletionErrorMessage = errorMessage,
      complete: () => this.allCpaResponse = this.allCpaResponse?.filter(item => item !== cpaId)
    });
  }

  resetLabelsToInitialValues() {
    this.activationErrorMessage = "";
    this.deletionErrorMessage = "";
  }

}
