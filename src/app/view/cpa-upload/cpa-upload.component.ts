import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { CPAService } from '../../shared/service/cpa.service';
import { flyInOut } from '../../shared/animation/app.animation';

@Component({
  selector: 'cpa-upload',
  templateUrl: './cpa-upload.component.html',
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut()
  ]
})

export class CpaUploadComponent implements OnInit {

  @ViewChild('uploadform') uploadFormDirective;

  cpaUploadForm!: FormGroup;

  validationErrorMessage!: string;
  uploadErrorMessage!: string;
  successLabel!: string;
  isDisabled = true;
  file: any;

  constructor(
    private cpaService: CPAService,
    private fb: FormBuilder) {
      this.createForm();
  }

  ngOnInit() { }

  createForm(): void {
    this.cpaUploadForm = this.fb.group({
      fileName: ['']
    });
  }

  validateCPA(event: any) {
    this.resetLabelsToInitialValues();
    this.file = event.target.files[0];
    
    let reader = new FileReader();
    reader.onload = () => {
      this.cpaService.validateCPA(reader.result).subscribe({
        error: (errorMessage) => this.validationErrorMessage = errorMessage,
        complete: () => this.isDisabled = false
      });
    };
    reader.readAsText(this.file, 'UTF-8')
  }

  onReset() {
    this.cpaUploadForm.reset();
    this.resetLabelsToInitialValues();
  }

  resetLabelsToInitialValues() {
    this.validationErrorMessage = "";
    this.uploadErrorMessage = "";
    this.successLabel = "";
    this.isDisabled = true;
  }

  onSubmit() {
    this.resetLabelsToInitialValues();
    let reader = new FileReader();
    reader.onload = () => {
      this.cpaService.insertCPA(reader.result).subscribe({
        error: (errorMessage) => this.uploadErrorMessage = errorMessage,
        complete: () => {
          this.successLabel = "CPA is met succes toegevoegd";
        }
      });
    };
    reader.readAsText(this.file, 'UTF-8')
  }

}
