import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title: string;
  menu: string;

  constructor() {
    this.title = 'OSDBK Admin Console';

    this.menu = JSON.stringify([
      {
        id: 'active',
        label: 'Actieve CPAs',
        href: './active',
      },
      {
        id: 'inactive',
        label: 'Inactieve CPAs',
        href: './inactive',
      },
      {
        id: 'upload',
        label: 'Uploaden',
        href: './upload',
      }
    ]);

  }

}
