// ANGULAR PROVIDED COMPONENTS
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ReactiveFormsModule, FormsModule } from '@angular/forms'; 
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// ROUTING
import { AppRoutingModule } from './routing/routing.module';

//INTERCEPTORS
import { BasicAuthInterceptor } from './shared/interceptor/basic.auth.interceptor';
import { ErrorInterceptor } from './shared/interceptor/error.interceptor';

//SERVICES
import { CPAService } from './shared/service/cpa.service';
import { EnvServiceProvider } from './shared/service/env.service.provider';

//CUSTOM COMPONENTS
import { AppComponent } from './app.component';
import { CpaActiveComponent } from './view/cpa-active/cpa-active.component';
import { CpaUploadComponent } from './view/cpa-upload/cpa-upload.component';
import { CpaInactiveComponent } from './view/cpa-inactive/cpa-inactive.component';
import { CpaViewComponent } from './view/cpa-view/cpa-view.component';
import { CpaErrorComponent } from './view/cpa-error/cpa-error.component';
import { ConfirmDialogComponent } from './shared/component/confirm-dialog/confirm-dialog.component';

//CUSTOM PIPES
import { CpaFilterPipe } from './shared/pipe/cpa-pipe/cpa-filter-pipe';

@NgModule({
  declarations: [
    AppComponent,
    CpaActiveComponent,
    CpaUploadComponent,
    CpaInactiveComponent,
    CpaViewComponent,
    CpaErrorComponent,
    ConfirmDialogComponent,
    CpaFilterPipe
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    MatButtonModule, 
    MatDialogModule,
    NgxPaginationModule
  ],
  providers: [
    CPAService,
    EnvServiceProvider,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
