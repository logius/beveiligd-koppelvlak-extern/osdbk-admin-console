import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { EnvService } from '../service/env.service'


@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {

    constructor(private env: EnvService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add header with basic auth credentials if request is to the api url
        const isApiUrl = request.url.startsWith(this.env.apiUrl);
        if (isApiUrl) {
            request = request.clone({
                setHeaders: { 
                    Authorization: `Basic ${this.env.authToken}`
                }
            });
        }

        return next.handle(request);
    }
}