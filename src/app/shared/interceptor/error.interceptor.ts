import {
    HttpEvent,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse,
    HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
export class ErrorInterceptor implements HttpInterceptor {
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(
                retry(1),
                catchError((error: HttpErrorResponse) => {
                    let errorMessage = '';
                    if (error.error instanceof ErrorEvent) {
                        // client-side error
                        if (!error.error) {
                            errorMessage = 'Error: Unknown error occurred';
                        } else {
                            errorMessage = `Error message: ${error.error.message}`;
                        }
                    } else {
                        // server-side error
                        if (!error.error) {
                            errorMessage = `Error status: ${error.status}, message: Unknown error occurred`;
                        } else {
                            errorMessage = `Error status: ${error.status}, message: ${error.error.message}`;
                        }
                    }
                    console.log(errorMessage);
                    return throwError(() => errorMessage);
                })
            )
    }
}