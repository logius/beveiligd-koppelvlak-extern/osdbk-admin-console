import { EnvService } from './env.service'
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CPAParty } from '../model/cpa-model';

@Injectable({ providedIn: 'root' })
export class CPAService {

  constructor(private http: HttpClient, private env: EnvService) { }

  getCPA(cpaId: string): Observable<string> {
    return this.http.get(this.env.apiUrl + "/" + cpaId, { responseType: 'text' });
  }

  downloadCPA(cpaId: string): Observable<Blob> {
    return this.http.get(this.env.apiUrl + "/" + cpaId, { responseType: 'blob' });
  }

  pingCpa(cpaId: string, fromPartyIdx: number, toPartyIdx: number): Observable<string> {
    return this.http.post(this.env.apiUrl + "/ping/" + cpaId+ "/fromIdx/" + fromPartyIdx + "/toIdx/" + toPartyIdx, null, {responseType: 'text'});
  }

  validateCPA(cpa: any): Observable<string> {
    return this.http.put<string>(this.env.apiUrl + "/validate", cpa);
  }

  insertCPA(cpa: any): Observable<string> {
    return this.http.post(this.env.apiUrl + "/insert", cpa, { responseType: 'text' });
  }

  deleteCPA(cpaId: string): Observable<string> {
    return this.http.delete<string>(this.env.apiUrl + "/" + cpaId);
  }

  getActiveCPAIds(): Observable<string[]> {
    return this.http.get<string[]>(this.env.apiUrl + "/active");
  }

  getInactiveCPAIds(): Observable<string[]> {
    return this.http.get<string[]>(this.env.apiUrl + "/inactive");
  }

  deactivateCPA(cpaId: string): Observable<string> {
    return this.http.put<string>(this.env.apiUrl + "/deactivate/" + cpaId, JSON.stringify({}));
  }

  activateCPA(cpaId: string): Observable<string> {
    return this.http.put<string>(this.env.apiUrl + "/activate/" + cpaId, JSON.stringify({}));
  }

  resendFailed(cpaId: string): Observable<string> {
    return this.http.put<string>(this.env.apiUrl + "/resend/failed/" + cpaId, JSON.stringify({}));
  }

  resendExpired(cpaId: string): Observable<string> {
    return this.http.put<string>(this.env.apiUrl + "/resend/expired/" + cpaId, JSON.stringify({}));
  }

  getCPAParty(cpaId: string): Observable<CPAParty[]> {
    return this.http.get<CPAParty[]>(this.env.apiUrl + "/" + cpaId + "/party");
  }


}
