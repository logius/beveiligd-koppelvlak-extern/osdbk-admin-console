export interface CpaRole {
  name: string;
  service: string;
  receiveActions: string[];
  sendActions: string[];
}

export interface CPAParty {
  partyId: string;
  partyName: string;
  cpaRoles: CpaRole[];
}
