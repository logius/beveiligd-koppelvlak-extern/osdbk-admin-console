(function(window) {
  window.__env = window.__env || {};
  // API url
  window.__env.apiUrl  = "${APIURL}" 
  window.__env.authToken = "${AUTHTOKEN}"
})(this);