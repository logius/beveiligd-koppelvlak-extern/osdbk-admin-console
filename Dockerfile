#Stage 0: "build-stage", based on Node.js, to build and compile the frontend
FROM bitnami/node:16 AS build-stage

# Set NPM config
RUN npm config set strict-ssl false
RUN npm config set registry  https://nexus.cicd-p-az1.l12m.nl/repository/pgu-npm-group/

# Copying all necessary files required for npm install
COPY package.json tsconfig.json tslint.json ./

# Install npm dependencies in a different folder to optimize container build process
RUN npm install

# Copy node modules 
RUN cp -R ./node_modules ./app

# Setting application directory as work directory
WORKDIR /app

# Copying application code to container application directory
COPY . .

# Building the angular app
RUN npm run build


#Stage 1: Setup nginx and Deploy application
FROM nginxinc/nginx-unprivileged:1.25.3

#Copy osdbk-admin-console-dist
COPY --from=build-stage /app/dist/app/ /usr/share/nginx/html/

#Copy default nginx configuration
COPY config/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080

CMD ["nginx","-g","daemon off;"]
